FROM ubuntu:latest
RUN apt-get -y update && apt-get autoremove -y &&  apt-get install -y python3-pip python3-dev build-essential && apt-get install -y supervisor
COPY app /app
COPY requirements.txt /app
RUN pip3 install -r app/requirements.txt
COPY app.conf /etc/supervisor/conf.d/
CMD ["/usr/bin/supervisord"]


