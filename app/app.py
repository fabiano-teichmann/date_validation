from flask import Flask
from convert_dates import ConvertDate
from pymongo import MongoClient

from datetime import datetime

app = Flask(__name__)


@app.route("/")
def hello():
    client = MongoClient('mongodb://0.0.0.0:27017')
    db = client.dates
    collection = db.date_brazil
    now = datetime.now()
    date = ConvertDate().convert_datetime_to_date_brazil(now)
    _id = collection.insert_one({'date': date})
    return f'Converteu do formato datetime {now} para: {date} e inseriu no mongo {_id}'


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
